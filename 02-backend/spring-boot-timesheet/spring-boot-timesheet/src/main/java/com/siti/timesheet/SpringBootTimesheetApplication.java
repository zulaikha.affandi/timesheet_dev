package com.siti.timesheet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTimesheetApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTimesheetApplication.class, args);
	}

}
